# Slowb.ro - Git

This is a repository for all news related items about the Slowb.ro Gitea hosting, as well as a way to reach the admin(s) about any ideas that you have for our Git service

## Latest News
- [2024-04-11 - Forgejo Migration (& New Domain Name)](news-2024-04-11.md)
- [2024-03-22 - Disallow Search Engines](news-2024-03-22.md)
- [2024-02-13 - Bots](news-2024-02-13.md)
- [2023-09-27 - Instabilities (500 errors)](news-2023-09-27.md)
- [2023-06-05 - No Downtime and Increases in Storage](news-2023-06-05.md)
- [2023-05-20 - New Theme & Transparency](news-2023-05-20.md)